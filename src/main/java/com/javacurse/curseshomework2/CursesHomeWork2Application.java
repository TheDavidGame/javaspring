package com.javacurse.curseshomework2;

import com.javacurse.curseshomework2.dao.BookDAOBean;
import com.javacurse.curseshomework2.dao.UserDAOBean;
import com.javacurse.curseshomework2.dbExample.model.Book;
import com.javacurse.curseshomework2.dbExample.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.time.LocalDate;
import java.util.Arrays;

@SpringBootApplication
public class CursesHomeWork2Application {

//	public CursesHomeWork2Application(UserDAOBean userDAOBean, BookDAOBean bookDAOBean) {
//		this.userDAOBean = userDAOBean;
//		this.bookDAOBean = bookDAOBean;
//	}

	public static void main(String[] args) {
		SpringApplication.run(CursesHomeWork2Application.class, args);
	}

//	private UserDAOBean userDAOBean;
//	private BookDAOBean bookDAOBean;

//	@Override
//	public void run(String... args) throws Exception {
//		User user = new User(1, "Иванов", "Иван", LocalDate.of(1990, 1, 1),
//				1234567890, "ivanov@mail.ru",
//				Arrays.asList(new Book(1,"Война и мир", 1),
//						new Book(2, "Преступление и наказание", 1)));
//		userDAOBean.addUser(user);
//		Integer userId = userDAOBean.getByMail("ivanov@mail.ru");
//		System.out.println(bookDAOBean.findBooksByUserId(userId));
//	}

//	@Autowired
//	public UserDAOBean getUserDAO(UserDAOBean userDAOBean){
//		return userDAOBean;
//	}
}
