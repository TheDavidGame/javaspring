package com.javacurse.curseshomework2.dbExample.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
public class Book {
    private Integer id;
    private String name;
    private Integer userId;
}
