package com.javacurse.curseshomework2.dbExample.model;

import lombok.*;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor

public class User {
    private Integer id;
    private String name;
    private String lastName;
    private LocalDate birth;
    private Integer phone;
    private String mail;
    private List<Book> books;

}
