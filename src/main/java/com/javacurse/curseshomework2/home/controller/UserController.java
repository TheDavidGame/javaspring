package com.javacurse.curseshomework2.home.controller;

import com.javacurse.curseshomework2.home.model.Users;
import com.javacurse.curseshomework2.home.repository.GenericRepository;
import com.javacurse.curseshomework2.home.repository.UserRepository;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;

@RestController
@RequestMapping("/users")
@Tag(name = "Пользователи", description = "Контроллер для работы с пользователям фильмотеки")
public class UserController
        extends GenericController<Users> {

    private final UserRepository userRepository;

    public UserController(GenericRepository<Users> genericRepository) {
        super(genericRepository);
        this.userRepository = (UserRepository) genericRepository;
    }

    @Operation(description = "Добавить пользователя")
    @PostMapping(value = "/addUsers",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Users> create(@RequestBody Users newEntity) {
        newEntity.setCreatedWhen(LocalDateTime.now());
        return ResponseEntity.status(HttpStatus.OK).body(userRepository.save(newEntity));
    }

}

