package com.javacurse.curseshomework2.home.controller;

import com.javacurse.curseshomework2.home.model.Films;
import com.javacurse.curseshomework2.home.repository.GenericRepository;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/films")
@Tag(name = "Фильмы", description = "Контроллер для работы с фильмами из фильмотеки")
public class FilmController
        extends GenericController<Films>{

    public FilmController(GenericRepository<Films> genericRepository) {
        super(genericRepository);
    }
}

