package com.javacurse.curseshomework2.home.controller;

import com.javacurse.curseshomework2.home.model.Orders;
import com.javacurse.curseshomework2.home.repository.GenericRepository;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/rent/info")
@Tag(name = "Аренда фильмов",
        description = "Контроллер для работы с арендой/выдачей фильм пользователям фильмотеки")
public class OrderController
        extends GenericController<Orders>{

    public OrderController(GenericRepository<Orders> genericRepository) {
        super(genericRepository);
    }
}

