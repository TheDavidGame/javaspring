package com.javacurse.curseshomework2.home.controller;

import com.javacurse.curseshomework2.home.model.Directors;
import com.javacurse.curseshomework2.home.model.GenericModel;
import com.javacurse.curseshomework2.home.repository.GenericRepository;
import io.swagger.v3.oas.annotations.Operation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.webjars.NotFoundException;


import java.util.List;

@RestController
@Slf4j
public abstract class GenericController<T extends GenericModel> {

    private final GenericRepository<T> genericRepository;

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    public GenericController(GenericRepository<T> genericRepository) {
        this.genericRepository = genericRepository;
    }

    @Operation(description = "Получить все записи", method = "getAll")
    @GetMapping(value = "/getAll", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<T>> getAll() {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(genericRepository.findAll());
    }

    @Operation(description = "получение записи по ID", method = "getOneById")
    @GetMapping(value = "/getOneById", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<T> getOneById(@RequestParam(value = "id") Long id) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(genericRepository.findById(id).orElseThrow(() -> new NotFoundException("Данные по заданному ID не найдены")));
    }

    @Operation(description = "Создать запись", method = "add")
    @PostMapping(value = "/add",
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<T> create(@RequestBody T newEntity) {
        log.info(newEntity.toString());
        genericRepository.save(newEntity);
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(newEntity);
    }

    @Operation(description = "Обновить запись", method = "update")
    @PutMapping(value = "/update",
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<T> update(@RequestBody T updatedEntity,
                                    @RequestParam(value = "id") Long id) {
        updatedEntity.setId(id);
        genericRepository.save(updatedEntity);
        return ResponseEntity
                .status(HttpStatus.ACCEPTED)
                .body(updatedEntity);
    }

    @Operation(description = "Удалить запись", method = "delete")
    @DeleteMapping(value = "/delete/{id}")
    public void delete(@PathVariable(value = "id") Long id) {
        genericRepository.deleteById(id);
    }


}
