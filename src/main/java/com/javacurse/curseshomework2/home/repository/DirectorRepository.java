package com.javacurse.curseshomework2.home.repository;


import com.javacurse.curseshomework2.home.model.Directors;
import org.springframework.stereotype.Repository;

@Repository
public interface DirectorRepository extends GenericRepository<Directors> {

}
