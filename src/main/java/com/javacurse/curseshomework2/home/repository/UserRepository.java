package com.javacurse.curseshomework2.home.repository;

import com.javacurse.curseshomework2.home.model.Users;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository
        extends GenericRepository<Users> {
}

