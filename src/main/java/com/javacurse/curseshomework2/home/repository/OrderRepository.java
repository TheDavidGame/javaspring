package com.javacurse.curseshomework2.home.repository;

import com.javacurse.curseshomework2.home.model.Orders;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderRepository
        extends GenericRepository<Orders> {
}

