package com.javacurse.curseshomework2.home.repository;

import com.javacurse.curseshomework2.home.model.Films;
import org.springframework.stereotype.Repository;

@Repository
public interface FilmRepository
        extends GenericRepository<Films> {
}

