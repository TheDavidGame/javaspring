package com.javacurse.curseshomework2.home.repository;

import com.javacurse.curseshomework2.home.model.Role;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository
        extends  GenericRepository<Role>{
}

