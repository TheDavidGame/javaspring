package com.javacurse.curseshomework2.home.model;

public enum Genre {
    FANTASY,
    DRAMA,
    ACTION,
    DOCUMENTARY
}
