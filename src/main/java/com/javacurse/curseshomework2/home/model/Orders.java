package com.javacurse.curseshomework2.home.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Entity
@Table(name = "orders")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Orders extends GenericModel {
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "default_generator")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "film_id", nullable = false,
            foreignKey = @ForeignKey(name = "FK_ORDERS_FILMS"))
    private Films films;

    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false,
    foreignKey = @ForeignKey(name = "FK_ORDERS_USERS"))
    private Users users;

    @Column(name = "rent_date", nullable = false)
    private LocalDateTime rentDate;

    @Column(name = "rent_period", nullable = false)
    private LocalDateTime rentPeriod;

    @Column(name = "purchcase", nullable = false)
    private Boolean purchcase;
}
