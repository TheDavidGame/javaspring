package com.javacurse.curseshomework2.dao;

import com.javacurse.curseshomework2.dbExample.model.User;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Date;

@Repository
public class UserDAOBean {
    private final Connection connection;
    private BookDAOBean bookDAOBean;

    private final String USER_SELECT_BY_ID = "select * from user_table where id = ?";
    private final String INSERT_INTO_USER = "insert into user_table (id, last_name, first_name, birth_date, phone, email) VALUES (?, ?, ?, ?, ?, ?)";
    private final String GETBYMAIL = "select id from user_table where email = ?";
    public UserDAOBean(Connection connection, BookDAOBean bookDAOBean) {
        this.connection = connection;
        this.bookDAOBean = bookDAOBean;
    }


    public User findUserById(Integer userId) throws SQLException {
        PreparedStatement selectQuery = connection.prepareStatement(USER_SELECT_BY_ID);
        selectQuery.setInt(1, userId);
        ResultSet resultSet = selectQuery.executeQuery();
        User user = new User();
        while (resultSet.next()) {
            user.setId(resultSet.getInt("id"));
            user.setName(resultSet.getString("first_name"));
            user.setLastName(resultSet.getString("last_name"));
            user.setBirth(resultSet.getDate("date_added").toLocalDate());
            System.out.println(user);
        }
        return user;
    }

    public void addUser(User user) throws SQLException {
        PreparedStatement selectQuery = connection.prepareStatement(INSERT_INTO_USER);
        selectQuery.setInt(1, user.getId());
        selectQuery.setString(2, user.getLastName());
        selectQuery.setString(3, user.getName());
        selectQuery.setDate(4, Date.valueOf(user.getBirth()));
        selectQuery.setInt(5, user.getPhone());
        selectQuery.setString(6, user.getMail());

        selectQuery.executeUpdate();


        user.getBooks()
                .forEach(book -> {
                    try {
                        bookDAOBean.addBook(book);
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                });

    }

    public int getByMail(String mail) throws SQLException{
        PreparedStatement selectQuery = connection.prepareStatement(GETBYMAIL);
        selectQuery.setString(1, mail);

        ResultSet resultSet = selectQuery.executeQuery();

        while (resultSet.next()){
            return resultSet.getInt("id");
        }
        return 0;
    }

}
