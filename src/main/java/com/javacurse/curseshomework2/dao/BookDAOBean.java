package com.javacurse.curseshomework2.dao;

import com.javacurse.curseshomework2.dbExample.model.Book;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;





@Component
public class BookDAOBean {
    private final Connection connection;
    private final String SELECT_BOOKS_BY_USERID = "select * from book_table where user_id = ?";
    private final String ADD_BOOK = "insert into book_table (id, book_name, user_id) VALUES (?, ?, ?)";
    public BookDAOBean(Connection connection) {
        this.connection = connection;
    }

    public List<String> findBooksByUserId(Integer userId) throws SQLException{
        PreparedStatement selectQuery = connection.prepareStatement(SELECT_BOOKS_BY_USERID);
        selectQuery.setInt(1, userId);
        ResultSet resultSet = selectQuery.executeQuery();

        List<String> result = new ArrayList<>();
        while(resultSet.next()){
            result.add(resultSet.getString("book_name"));
        }

        return result;
    }

    public void addBook(Book book) throws SQLException{
        PreparedStatement selectQuery = connection.prepareStatement(ADD_BOOK);
        selectQuery.setInt(1, book.getId());
        selectQuery.setString(2, book.getName());
        selectQuery.setInt(3, book.getUserId());

        selectQuery.executeUpdate();

    }
}
